package eu.mytthew;

public class Beetle {
	int x = 0;
	int y = 0;
	char symbol = '>';

	public char getSymbol() {
		return symbol;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void move(int x, int y) {
		this.x += x;
		this.y += y;
	}

	public void transform(char symbol) {
		this.symbol = symbol;
	}

	@Override
	public String toString() {
		return "Beetle[symbol='" + symbol + "', x=" + x + ", y=" + y + "]";
	}
}
