package eu.mytthew;

public abstract class BeetleCommand {
	protected Beetle beetle;

	public BeetleCommand(Beetle beetle) {
		this.beetle = beetle;
	}

	abstract void execute();

	abstract void undo();

	abstract void redo();
}
