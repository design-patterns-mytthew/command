package eu.mytthew;

public class BeetleMove extends BeetleCommand {
	private final int newX;
	private final int newY;
	private int currentX;
	private int currentY;

	public BeetleMove(Beetle beetle, int newX, int newY) {
		super(beetle);
		this.newX = newX;
		this.newY = newY;
	}

	@Override
	public void execute() {
		beetle.move(newX, newY);
	}

	@Override
	public void undo() {
		currentX = beetle.x;
		currentY = beetle.y;
		beetle.move(-newX, -newY);
	}

	@Override
	void redo() {
		beetle.move(currentX, currentY);
	}
}
