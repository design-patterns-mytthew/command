package eu.mytthew;

public class BeetleTransform extends BeetleCommand {
	char oldSymbol;
	char newSymbol;

	public BeetleTransform(Beetle beetle, char newSymbol) {
		super(beetle);
		this.newSymbol = newSymbol;
	}

	@Override
	void execute() {
		oldSymbol = beetle.getSymbol();
		beetle.transform(newSymbol);
	}

	@Override
	void undo() {
		beetle.transform(oldSymbol);
	}

	@Override
	void redo() {
		beetle.transform(newSymbol);
	}
}
