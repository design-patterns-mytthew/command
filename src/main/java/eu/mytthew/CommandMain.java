package eu.mytthew;

import java.util.Scanner;
import java.util.Stack;

public class CommandMain {
	public static void main(String[] arguments) {
		System.out.print("> ");
		Beetle beetle = new Beetle();
		Stack<BeetleCommand> beetleUndo = new Stack<>();
		Stack<BeetleCommand> beetleRedo = new Stack<>();
		Scanner scanner = new Scanner(System.in);

		String line;
		while ((line = scanner.nextLine()).length() != 0) {
			System.out.println("\"" + line + "\", " + line.length());
			String[] args = line.split(" ");
			switch (args[0].charAt(0)) {
				case 'm':
					BeetleCommand beetleMove = new BeetleMove(beetle, Integer.parseInt(args[1]), Integer.parseInt(args[2]));
					beetleUndo.add(beetleMove);
					beetleMove.execute();
					break;
				case 't':
					BeetleCommand beetleTransform = new BeetleTransform(beetle, args[1].charAt(0));
					beetleUndo.add(beetleTransform);
					beetleTransform.execute();
					break;
				case 'u':
					BeetleCommand undo = beetleUndo.remove(beetleUndo.size() - 1);
					beetleRedo.add(undo);
					undo.undo();
					break;
				case 'r':
					BeetleCommand redo = beetleRedo.remove(beetleRedo.size() - 1);
					redo.redo();
					beetleUndo.add(redo);
					break;
				default:
					break;
			}
			System.out.println(beetle);
			System.out.print("> ");
		}
	}
}
